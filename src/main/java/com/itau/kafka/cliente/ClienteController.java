package com.itau.kafka.cliente;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class ClienteController {

    @Autowired
    private ClienteProducer clienteProducer;

    @Autowired
    private ClienteService clienteService;

    @GetMapping("/acesso/{cliente}/{porta}")
    public void verificaAcesso(@PathVariable int cliente, @PathVariable String porta) {
        Cliente clienteNovo = new Cliente();
        clienteNovo.setId(cliente);
        clienteNovo.setPorta(porta);
        clienteNovo.setTemAcesso(clienteService.verificarAcessoPorta(clienteNovo));
        clienteProducer.enviarAoKafka(clienteNovo);
    }

}
