package com.itau.kafka.cliente;

public class Cliente {

    private int id;
    private String porta;
    private boolean temAcesso;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPorta() {
        return porta;
    }

    public void setPorta(String porta) {
        this.porta = porta;
    }

    public boolean isTemAcesso() {
        return temAcesso;
    }

    public void setTemAcesso(boolean temAcesso) {
        this.temAcesso = temAcesso;
    }
}
