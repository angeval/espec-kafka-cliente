package com.itau.kafka.cliente;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class ClienteProducer {

    @Autowired
    private KafkaTemplate<String, String> producer;

    public void enviarAoKafka(Cliente cliente) {
        String log = "cliente " + cliente.getId() + ";" + " porta " + cliente.getPorta() + ";" + " acesso = " +
                cliente.isTemAcesso() + ";";
        producer.send("spec2-angela-valentim-1", log);
    }

}