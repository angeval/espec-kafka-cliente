package com.itau.kafka.cliente;

import org.springframework.stereotype.Service;

@Service
public class ClienteService {

    public boolean verificarAcessoPorta(Cliente cliente){

        if (cliente.getPorta().endsWith("0") || cliente.getPorta().endsWith("1")){
            return true;
        }
        return false;
    }
}
